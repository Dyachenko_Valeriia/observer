package main;

public interface Observer {
    String update(String message);
}
