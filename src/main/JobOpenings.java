package main;

import java.util.ArrayList;
import java.util.List;

public class JobOpenings {
    private List<String> jobs = new ArrayList<>();
    private List<User> users = new ArrayList<>();

    public void addUser(User user){
        users.add(user);
    }

    public void remove(User user){
        users.remove(user);
    }

    public List<String> addJob(String job) {
        jobs.add(job);
        return notifyObservers(job);
    }

    private List<String> notifyObservers(String job) {
        List<String> listJob = new ArrayList<>();
        for (User user : users) {
            listJob.add(user.update(job));
        }
        return listJob;
    }
}
