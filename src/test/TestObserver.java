package test;

import main.JobOpenings;
import main.User;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestObserver {
    @Test
    public void testObserver(){
        JobOpenings jobOpenings = new JobOpenings(); //Людей оповещают о новых вакансиях
        User user = new User();
        User user1 = new User();
        jobOpenings.addUser(user);
        jobOpenings.addUser(user1);
        List<String> expected = new ArrayList<>();
        Collections.addAll(expected, "Появилась новая вакансия: Программист", "Появилась новая вакансия: Программист");
        assertEquals(expected, jobOpenings.addJob("Программист"));
    }
}
